
public class Estudiante {
    String codigo;
	String nombre;
    Integer nota;
	public Estudiante (){
	}
	public Estudiante (String codigo,String nombre, Integer nota){
		this.codigo = codigo;
		this.nombre = nombre;
		this.nota = nota;
	}
	public String obtenerCodigo (){
		return this.codigo;
	} 
	public String obtenerNombre (){
		return this.nombre;
	}
	public Integer obtenerNota (){
		return this.nota;
	}  
}

