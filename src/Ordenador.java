import java.util.HashMap; 	
import java.util.Map;

public class Ordenador {

	Map<String, Ordenar> ordenarPor;
	public Ordenador() {
		ordenarPor = new HashMap<String, Ordenar>();
	}
	public void registrar (String o_por,Ordenar ordenar){
		ordenarPor.put(o_por, ordenar);
	}
	
	public void ejecutar (Estudiante[] estudiantes,String o_por){
		Ordenar orden = ordenarPor.get(o_por);
		orden.ordenar(estudiantes);
	}

}
